# Inherit the full_base and device configurations
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, device/freedesktop/rockpi/rockpi4/device.mk)
$(call inherit-product, device/freedesktop/rockpi/device-common.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base.mk)

# Product overrides
PRODUCT_NAME := rockpi4
PRODUCT_DEVICE := rockpi4
PRODUCT_BRAND := Android
